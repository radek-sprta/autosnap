#!/bin/bash
#
# Keeps a given number a snapshots for a btrfs subvolume 
#
# Example crontab:
# 0 * * * * /usr/local/bin/autosnap /home hourly 6
# 0 0 * * * /usr/local/bin/autosnap /home daily 7
# 0 0 * * 0 /usr/local/bin/autosnap / weekly 4
#
# Author:   Radek SPRTA
# Date:     2015/07/26
# License:  Modified BSD 3-clause To see, use option: -l

export PATH=/bin:/sbin:/usr/bin/

readonly AUTHOR="Radek SPRTA"
readonly SUBVOLUME="$1"
readonly PREFIX="$2"
readonly KEEP="$3"
readonly MOUNT_DIR=/mnt/autosnap
readonly SNAPS_DIR=snapshots
FULL_NAME=

show_license() {
    printf "
\"Revised BSD License\" (3-clause) for %s :
====== License Start ======
Copyright (c) 2015, $AUTHOR
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the $AUTHOR nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
\"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL $AUTHOR BE 
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  
====== End License ======

" $(basename $0) >&2
   exit 1
}

show_usage() {
  printf "
Usage: $0 SUBVOLUME SNAPSHOT_PREFIX SNAPSHOTS_TO_KEEP

Options: \n
-h, --help         show this help and exit
-l,                show license and exit
" $(basename $0) >&2
  exit 1
}

error() {
  echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@" >&2
}

# Unmounts the snapshots filesystem and deletes the mount point
clean() {
  sudo umount "${MOUNT_DIR}"
  sudo rmdir "${MOUNT_DIR}"
}

while getopts 'lh' option; do
  case ${option} in
    l) show_license ;;
    h) show_usage ;;
    *) show_usage ;;
  esac
done

if [[ "$#" -ne 3 ]]; then
  show_usage
fi

if [[ ! "${KEEP}" -eq "${KEEP}" ]]; then
  error "Number of snapshots to keep must be a number"
  exit 2
fi

# Get path to snapshots
# ${SUBVOLUME} has slash in it, so it is not needed before it
if [[ "${SUBVOLUME}" = "/" ]]; then
  FULL_NAME="${MOUNT_DIR}/${SNAPS_DIR}/root_${PREFIX}_"
else
  FULL_NAME="${MOUNT_DIR}/${SNAPS_DIR}${SUBVOLUME}_${PREFIX}_"
fi
readonly FULL_NAME

if [[ -d "${MOUNT_DIR}" ]]; then
  error "Directory ${MOUNT_DIR} must not be in use" 
  exit 2
fi

# Gets the device_path of the subvolume's filesystem
device_path=$(grep "${SUBVOLUME} " /etc/fstab | sed '/\#/d' | cut --fields=1 --delimiter=" ")
if [[ -z "${device_path}" ]]; then
  error "Unable to find device path of ${SUBVOLUME}"
  exit 2 
fi
sudo mkdir "${MOUNT_DIR}"
sudo mount "${device_path}" "${MOUNT_DIR}"
if [[ "$?" -ne 0 ]]; then
  error "Unable to mount the filesystem of ${SUBVOLUME}"
  clean
  exit 2
fi

if [[ ! -d "${MOUNT_DIR}/${SNAPS_DIR}" ]]; then
  sudo mkdir "${MOUNT_DIR}/${SNAPS_DIR}"
fi

sudo btrfs subvolume snapshot -r "${SUBVOLUME}" "${FULL_NAME}$(date +%F_%T.%N)"
if [[ "$?" -ne 0 ]]; then
  error "Unable to take a snapshot of ${SUBVOLUME}"
  clean
  exit 2
fi

num_snaps=$(ls -1 --directory "${FULL_NAME}"* | sort | wc --lines)
if [[ -z "${num_snaps}" ]]; then
  error "Unable to get the number of snapshots of ${SUBVOLUME}"
  clean
  exit 2 
fi
if [[ "${num_snaps}" -gt "${KEEP}" ]]; then
  let over=$num_snaps-${KEEP}
  while read snapshot; do
    sudo btrfs subvolume delete "${snapshot}"
  done < <(ls -1 --directory "${FULL_NAME}"* | head --lines="${over}")
fi

clean
